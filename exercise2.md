# Use Cases
### Сценарий №1: авторизация через получение кода в СМС
- Как: пользователь с выходом в Интернет, имеющий активную сим карту и моб.телефон
- Чтобы: авторизоваться в Сбермаркете и пользоваиться услугами сервиса

1. Дано: я нахожусь в модальном окне авторизации
2. Чтобы авторизоваться:
- Мне нужно правильно ввести действующий в РФ номер телефона в соответствии с форматом указанным в поле ввода, чтобы номер был активен и на него могло прийти смс
- мне нужно нажать на кнопку **"Получить код в СМС"**
- мне нужно немного подождать и найти смс в телефоне
- мне нужно ввести полученный в СМС код в новом модальном окне
3. Когда все будет сделано:
- моя авторизация пройдет успешно и осуществится вход на сайт https://sbermarket.ru/

### Сценарий №2: Вход через **VK**
- Как: пользователь с выходом в Интернет, зарегистрированный в социальной сети Вконтакте
- Чтобы: авторизоваться в Сбермаркете и пользоваиться услугами сервиса

1. Дано: я нахожусь в модальном окне авторизации
2. Чтобы авторизоваться через **VK**:
- мне нужно нажать на кнопку **"VK"**
- мне нужно дождаться открытия нового окна авторизации через Вконтакте - https://oauth.vk.com/
- мне нужно ввести в поле **Телефон или email** логин от действующей учетки соц сети Вконтакте
- мне нужно ввести пароль
- мне нужно нажать на кнопку **Войти**
3. Когда все будет сделано:
- моя авторизация пройдет успешно и осуществится вход на сайт https://sbermarket.ru/

### Сценарий №3: Вход через **Mail**
- Как: пользователь с выходом в Интернет, зарегистрированный в сервисе MAIL.RU, имеющий действующий почтовый ящик
- Чтобы: авторизоваться в Сбермаркете и пользоваиться услугами сервиса

1. Дано: я нахожусь в модальном окне авторизации
2. Чтобы авторизоваться через Mail:
- мне нужно нажать на кнопку **""@""**
- мне нужно дождаться открытия нового окна авторизации через MAIL.RU - https://oauth.mail.ru/
- мне нужно в новом окне нажать на кнопку **РАЗРЕШИТЬ**, т.е. Разрешить приложению sbermarket доступ к нашим данным в MAIL.RU
3. Когда все будет сделано:
- моя авторизация пройдет успешно и осуществится вход на сайт https://sbermarket.ru/

### Сценарий №4: Получение выгодных предложений
- Как: пользователь с выходом в Интернет, желающий зарегистрироваться в сервисе sbermarket и при этом получать от сервиса выгодные предложения 
- Чтобы: авторизоваться в Сбермаркете и получать выгодные проедложения сервиса

1. Дано: я нахожусь в модальном окне авторизации
2. Чтобы получать выгодные предложения сервиса:
- мне нужно нажать на фразу "выгодные предложения" и ознакомиться с предложениями
- мне нужно проверить, что в чек-боксе установлена галочка
- мне нужно авторизоваться в сервисе одним из возможных способов
3. Когда все будет сделано:
- моя авторизация на сайт https://sbermarket.ru/ пройдет успешно и вместе с тем буду получать рассылки и предложения от сервиса

### Сценарий №5: Вход по СберID
- Как: пользователь с выходом в Интернет, желающий зарегистрироваться в сервисе sbermarket
- Чтобы: авторизоваться в сервисе sbermarket через его собственный бесплатный сервис для входа на сайты и в приложения - СберID
1. Дано: я нахожусь в модальном окне авторизации
2. Чтобы авторизоваться через **СберID**:
- мне нужно нажать на кнопку **"Войти по Сбер ID"**
- мне нужно дождаться открытия нового окна авторизации
- мне нужно ввести номер телефона
- мне нужно нажать на кнопку **Войти или создать Сбер ID**
3. Когда все будет сделано:
- моя авторизация пройдет успешно и осуществится вход на сайт https://sbermarket.ru/

### Сценарий №6: Хочу заказывать для бизнеса
- Как: пользователь с выходом в Интернет, желающий зарегистрироваться в сервисе sbermarket, и у которого есть аккаунт в СберБизнес
- Чтобы: авторизоваться в сервисе sbermarket и заказывать товары для Бизнес целей
1. Дано: я нахожусь в модальном окне авторизации
2. Чтобы авторизоваться через **СберБизнес**:
- мне нужно нажать чек-бокс "Хочу заказывать для Бизнеса" (установить галочку)
- мне нужно нажать на кнопку **Войти по СберБизнесID** и  подождать открытия новой страницы
- мне нужно ввести мои логин и пароль от СберБизнес
3. Когда все будет сделано:
- моя авторизация пройдет успешно и я смогу заказывать товары для бизнеса в сервисе sbermarket

### Сценарий №7: Правила работы сервиса
- Как: пользователь с выходом в Интернет, желающий ознакомиться с правилами работы сервиса sbermarket
- Чтобы: ознакомиться с правилами работы сервиса перед авторизацией
1. Дано: я нахожусь в модальном окне авторизации
2. Чтобы правила работы сервиса:
- мне нужно прокрутить окно авторизации вниз до конца 
- мне нужно нажать на кнопку (надпись) **правилами работы сервиса** и подождать открытия страницы
3. Когда все будет сделано:
- откроется страница с правилами, и я смогу с ними ознакомиться перед авторизацией

### Сценарий №8: Политика обработки Персональных данных
- Как: пользователь с выходом в Интернет, желающий ознакомиться с документом политики обработки ПДн сервиса sbermarket - Приказ №П-2023-03-01
- Чтобы: ознакомиться с политикой обработки ПНд сервиса перед авторизацией
1. Дано: я нахожусь в модальном окне авторизации
2. Чтобы правила работы сервиса:
- мне нужно прокрутить окно авторизации вниз до конца 
- мне нужно нажать на кнопку (надпись) **политикой обработки ПДн** и подождать открытия страницы
3. Когда все будет сделано:
- откроется новая вкладка с документом в формате PDF Приказа,  и я смогу с ними ознакомиться 
